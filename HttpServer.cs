﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace web_thing
{
    class HttpServer
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(HttpServer));

        private string Path { get; }
        private readonly HttpListener listener = new HttpListener();

        public HttpServer(string path)
        {
            Path = path;
        }

        private class RequestTracker
        {
            public Guid Guid { get; set; }

            public override string ToString()
            {
                return Guid.ToString();
            }
        }

        public void Start()
        {
            listener.Prefixes.Add(Path);
            listener.Start();
            log.Info("server initialized");
            log.Error("ERROR");
            log.Debug("DEBUG");
            log.Fatal("FATAL");


            var tracker = new RequestTracker();
            log4net.GlobalContext.Properties["request-id"] = tracker;
            while (true)
            {
                try
                {
                    tracker.Guid = Guid.NewGuid();
                    var ctx = listener.GetContext();
                    ProcessRequest(ctx, tracker.Guid);
                }
                catch (Exception ex)
                {
                    log.Error("Request error", ex);
                }
            }
        }

        private static List<string> ProcessUrlSegments(string absolutePath)
        {
            var elements = absolutePath.Split('/');
            var segments = new List<string>();

            if (elements.Length > 0)
            {
                segments = elements.ToList();
            }
            return segments;
        }

        private void ProcessRequest(HttpListenerContext ctx, Guid requestID)
        {
            var urlSegments = ProcessUrlSegments(ctx.Request.Url.AbsolutePath);

            var responseText = new StringBuilder();
            urlSegments.ForEach(t => responseText.Append(t));

            var code = ctx.Response.StatusCode;

            var message = $"{code} {ctx.Request.HttpMethod} {ctx.Request.Url.AbsolutePath} ";
            if (code >= 200 && code < 400)
            {
                log.Info(message);
            }
            else
            {
                log.Error(message);
            }

            var writer = new StreamWriter(ctx.Response.OutputStream);
            writer.AutoFlush = true;
            writer.WriteLine(responseText.ToString());
            ctx.Response.Close();
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }
    }
}
