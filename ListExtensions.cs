﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace web_thing
{
    public static class ListExtensions
    {
        public static void Add<T1, T2>(this ICollection<KeyValuePair<T1, T2>> target, T1 Item1, T2 Item2)
        {
            if (target == null)
                throw new ArgumentException(nameof(target));

            target.Add(new KeyValuePair<T1, T2>(Item1, Item2));
        }
    }
}
