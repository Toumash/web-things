﻿using System;


namespace web_thing
{
    internal static class Program
    {
        static void Main(string[] args)
        {
            string input = string.Empty;
#if DEBUG
            input = "localhost";
#else
            Console.Write("IP to bind: ");
            input = Console.ReadLine();
#endif
            var srv = new HttpServer($"http://{input}:8080/");
            srv.Start();
        }
    }
}
